from setuptools import setup

setup(
    name='app',
    packages=['app'],
    include_package_data=True,
    install_requires=[
        'flask',
    ],
    version='1.0.0',
    author='Rhistina Revilla',
    author_email='rhistina@gmail.com',
    description="A Flask Webapp to display portfolio positions with option to filter records",
)