drop table if exists position;

create table position (
	date,
	portfolio_nbr,
	security_nbr,
	trade_qty)
;

drop table if exists security;

create table security (
	date,
	security_nbr,
	security_desc,
	price,
	multiplier
);

drop view if exists v_pos_mv_by_date;
create view v_pos_mv_by_date as
	select 	p.date,
			p.portfolio_nbr,
			p.security_nbr,
			p.trade_qty,
			(p.trade_qty*s.price)/s.multiplier as MV
	from position p , security s
	where p.security_nbr = s.security_nbr
	and p.date = s.date;