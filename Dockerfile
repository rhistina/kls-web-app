# Using an image that already has dependencies
FROM tiangolo/uwsgi-nginx-flask:flask

# Get kls-web-app code
RUN git clone https://rhistina@bitbucket.org/rhistina/kls-web-app.git

# Work directory
WORKDIR kls-web-app/app
ENV FLASK_APP "main.py"

# Pre-existing image is configured to have ROOT /app/static
# Make soft link so that the page renders with css
RUN ln -s /app/kls-web-app/app/static /app/static

# Initialize database then do one time data loads with sample data
RUN flask initdb
RUN cd ../loaders && python position_loader.py && python security_loader.py
