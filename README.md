# KLS-WEB-APP#

A Flask web application that displays portfolio positions and their market value on a date. 

### Description ###
A Flask web application in Python. Upon first visit to the page, users will see ALL available portfolio positions and their market value on a date. Users have the option to filter for positions greater than a specified market value. A clear button is provided to access main page and view all positions. A back button is available to go to last query. 

Due to time constraint, this is **NOT** a single page web application. It is just a simple Flask web application using regular Jinja2 templating. 

####TO DO####

* Make into a SPA by leveraging AngularJS or JQuery
* Leverage configuration file 
* Need test suite 

## Install and Configuration ##
Currently works in both Python 2.7 and 3.5. 

### Via Docker Image ###
In the same directory as the Dockerfile

Build the image
```
#!python

sudo docker build -t rhistina-kls-web-app .

```

Run the web server

```
#!python

sudo docker run -p 80:80 -t rhistina-kls-web-app

```



### Via Virtual Environment ###

To install on your own virtual environment:
```
#!python
# Clone repository
git clone git@bitbucket.org:rhistina/kls-web-app.git

# Go to project directory
cd kls-web-app

# Install the app
pip install --editable .

# Set FLASK_APP variable to where app is. This is relative to where your current directory is. 
# Feel free to put to entire path to app.py
export FLASK_APP=app/app.py

# Initalize DB. Clear tables and recreate them
flask initdb

# Load tables with sample data
cd loaders
python position_loader.py
python security_loader.py

# Go back up one folder
cd ..

# Run flask
flask run

```

### Example ###

For a live demo, please check out http://ec2-54-71-234-222.us-west-2.compute.amazonaws.com/

#### First Page ####
![Scheme](FirstPage.png)
#### Filtered Page ####
![Scheme](FilteredPage.png)


### Contact ###

* Rhistina Revilla (rhistina@gmail.com)