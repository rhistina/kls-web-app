from flask import Flask, render_template, request, url_for, g
import sqlite3
import os

# Initialize the Flask application
app = Flask(__name__)
app.config.update(dict(
    # LOCAL WORKSPACE TEST
    #DATABASE=os.path.join(app.root_path, 'database/psn.db'),
    # Empty DB
    DATABASE=os.path.join(app.root_path, 'psn.db'),
    DEBUG=True,
))

def connect_db():
    """Connects to the specific database."""
    rv = sqlite3.connect(app.config['DATABASE'])
    rv.row_factory = sqlite3.Row
    return rv


def init_db():
    """Initializes the database."""
    db = get_db()
    with app.open_resource('schema.sql', mode='r') as f:
        db.cursor().executescript(f.read())
    db.commit()


@app.cli.command('initdb')
def initdb_command():
    """Creates the database tables"""
    init_db()
    print('Initialized the database.')


def get_db():
    """Opens a new database connection if there is none yet for the current application context. """
    if not hasattr(g, 'sqlite_db'):
        g.sqlite_db = connect_db()
    return g.sqlite_db


@app.teardown_appcontext
def close_db(error):
    """Closes the database again at the end of the request."""
    if hasattr(g, 'sqlite_db'):
        g.sqlite_db.close()


@app.route('/', methods=['GET'])
def home():
    """Define a route for the default URL"""
    db = get_db()
    r = db.cursor().execute("select * from v_pos_mv_by_date" )
    result = r.fetchall()

    return render_template('template.html', resultset=result, title="All", h1_header="All available positions")

@app.route('/filter/', methods=['POST'])
def filter():
    """Define a route for user action"""
    mv_amt=request.form['mv_amt']
    db = get_db()
    r= db.cursor().execute("select * from v_pos_mv_by_date where MV>?" ,(int(mv_amt),) )
    result = r.fetchall()

    return render_template('template.html', resultset=result, mv_amt=mv_amt, title="Filtered", h1_header="Positions with market value greater than")

# Run the app if file called directly
if __name__ == '__main__':
    app.run()
