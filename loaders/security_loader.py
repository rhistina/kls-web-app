import csv, sqlite3
import os

SOURCE_DIRECTORY="test_data/"
SOURCE_FILE="securities.csv"

TARGET_DIRECTORY = "../app/"
TARGET_DB = "psn.db"

data_file = os.path.join(SOURCE_DIRECTORY, SOURCE_FILE)
db = os.path.join(TARGET_DIRECTORY, TARGET_DB)
conn = sqlite3.connect(db)
c = conn.cursor()

csv.register_dialect('piper', delimiter='|', quoting=csv.QUOTE_NONE)
with open(data_file, "r") as csvfile:
    for row in csv.DictReader(csvfile, dialect='piper'):
        security_record = (row['DATE'],row['SECURITY_NBR'], row['SECURITY_DESC'], row['PRICE'], row['MULTIPLIER'])
        c.execute('INSERT INTO security VALUES (?,?,?,?,?)', security_record)

conn.commit()
c.close()